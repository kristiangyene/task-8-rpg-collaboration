package main.java.characters.Support;
// Imports
import main.java.basestats.CharacterBaseStatsDefensive;
import main.java.characters.abstractions.HealSupport;
import main.java.characters.abstractions.ShieldSupport;
import main.java.items.armor.abstractions.ArmorType;
import main.java.spells.abstractions.ShieldingSpell;

/*
 Priest are the servants of the light and goodness.
 They use holy magic to heal and shield allies.
 As a support class they only have defensive stats.
*/
public class Priest extends ShieldSupport {

    // Base stats defensive
    private double baseHealth = CharacterBaseStatsDefensive.PRIEST_BASE_HEALTH;
    private double basePhysReductionPercent = CharacterBaseStatsDefensive.PRIEST_BASE_PHYS_RED; // Armor
    private double baseMagicReductionPercent = CharacterBaseStatsDefensive.PRIEST_BASE_MAGIC_RES; // Magic armor

    // Constructor
    public Priest() {
        // When the character is created it has maximum health (base health)
        this.currentHealth = baseHealth;
    }

    public Priest(ShieldingSpell shieldingSpell) {
        this.currentHealth = baseHealth;
        this.shieldingSpell = shieldingSpell;
    }

    // Public getters statuses and stats
    public double getCurrentMaxHealth() {
        return baseHealth;
    }  // Needs alteration after equipment is in

    // Character behaviours
    /**
     * Shields a party member for a percentage of their maximum health.
     * @param partyMemberMaxHealth
     */
    public double shieldPartyMember(double partyMemberMaxHealth) {
        return (partyMemberMaxHealth * shieldingSpell.getAbsorbShieldPercentage()) + (this.weapon.getWeaponModifier() * this.weapon.getRarityModifier());
    }

}
