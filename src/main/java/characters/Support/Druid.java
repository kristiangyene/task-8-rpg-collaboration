package main.java.characters.Support;
// Imports
import main.java.basestats.CharacterBaseStatsDefensive;
import main.java.characters.abstractions.HealSupport;
import main.java.items.armor.abstractions.ArmorType;
import main.java.spells.abstractions.HealingSpell;

/*
 Class description:
 ------------------
 Druids are spell casters who use nature based magic to aid their allies in battle.
 They can heal their allies or protect them using the forces of nature.
 As a support class they only have defensive stats.
*/
public class Druid extends HealSupport {

    // Base stats defensive
    private double baseHealth = CharacterBaseStatsDefensive.DRUID_BASE_HEALTH;
    private double basePhysReductionPercent = CharacterBaseStatsDefensive.DRUID_BASE_PHYS_RED; // Armor
    private double baseMagicReductionPercent = CharacterBaseStatsDefensive.DRUID_BASE_MAGIC_RES; // Magic armor


    // Constructor
    public Druid() {
        // When the character is created it has maximum health (base health)
        this.currentHealth = baseHealth;

    }

    public Druid(HealingSpell healingSpell) {
        this.currentHealth = baseHealth;
        this.healingSpell = healingSpell;
    }

    // Public getters statuses and stats
    public double getCurrentMaxHealth() {
        return baseHealth;
    }  // Needs alteration after equipment is in



}
