package main.java.characters.abstractions;

import main.java.items.armor.abstractions.ArmorType;
import main.java.items.weapons.abstractions.WeaponCategory;
import main.java.spells.abstractions.HealingSpell;

public abstract class HealSupport extends Character{
    // Metadata
    public final CharacterCategory CHARACTER_CAT = CharacterCategory.Support;
    public final ArmorType ARMOR_TYPE = ArmorType.Leather;
    public final WeaponCategory WEAPON_TYPE = WeaponCategory.Magic;
    public HealingSpell healingSpell;


    public void equipSpell(HealingSpell healingSpell){
        this.healingSpell = healingSpell;
    }


    // Character behaviours
    /**
     * Heals the party member
     */
    public double healPartyMember() {
        return this.healingSpell.getHealingAmount() * this.weapon.getWeaponModifier()
                * this.weapon.getRarityModifier();
        //return healingDone; // Return healing amount based on spell and modifiers.
    }

}
