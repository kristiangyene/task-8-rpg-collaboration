package main.java.characters.abstractions;

import main.java.items.armor.abstractions.ArmorType;
import main.java.items.weapons.abstractions.WeaponCategory;
import main.java.spells.abstractions.DamagingSpell;

public abstract class Caster extends Character {
    // Metadata
    public final CharacterCategory CHARACTER_CAT = CharacterCategory.Caster;
    public final ArmorType ARMOR_TYPE = ArmorType.Cloth;
    public final WeaponCategory WEAPON_TYPE = WeaponCategory.Magic;
    public DamagingSpell damagingSpell;

    public void equipSpell(DamagingSpell damagingSpell){
        this.damagingSpell = damagingSpell;
    }

}
