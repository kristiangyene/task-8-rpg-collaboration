package main.java.characters.melee;
// Imports
import main.java.basestats.CharacterBaseStatsDefensive;
import main.java.basestats.CharacterBaseStatsOffensive;
import main.java.characters.abstractions.CharacterCategory;
import main.java.characters.abstractions.Melee;
import main.java.items.armor.abstractions.ArmorType;
import main.java.items.armor.Leather;
import main.java.items.weapons.abstractions.WeaponCategory;
/*
 Rogues are stealthy combatants of the shadows.
 They wield bladed weapons with great agility and dispatch their enemies swiftly.
*/
public class Rogue extends Melee {
    // Metadata
    public final ArmorType ARMOR_TYPE = ArmorType.Leather;
    public final WeaponCategory WEAPON_TYPE = WeaponCategory.BladeWeapon;

    // Base stats defensive
    private double baseHealth = CharacterBaseStatsDefensive.ROGUE_BASE_HEALTH;
    private double basePhysReductionPercent = CharacterBaseStatsDefensive.ROGUE_BASE_PHYS_RED; // Armor
    private double baseMagicReductionPercent = CharacterBaseStatsDefensive.ROGUE_BASE_MAGIC_RES; // Magic armor
    // Base stats offensive
    private double baseAttackPower = CharacterBaseStatsOffensive.ROGUE_MELEE_ATTACK_POWER;


    // Constructor
    public Rogue() {
        // When the character is created it has maximum health (base health)
        this.currentHealth = baseHealth;
    }

    // Public getters statuses and stats
    public double getCurrentMaxHealth() {
        return baseHealth;
    }  // Needs alteration after equipment is in


    // Character behaviours
    /**
     * Damages the enemy
     */
    public double attackWithBladedWeapon() {
        return this.baseAttackPower * this.weapon.getWeaponModifier() * this.weapon.getRarityModifier();
    }

}
