package main.java.characters.melee;
// Imports
import main.java.basestats.CharacterBaseStatsDefensive;
import main.java.basestats.CharacterBaseStatsOffensive;
import main.java.characters.abstractions.CharacterCategory;
import main.java.characters.abstractions.Melee;
import main.java.items.armor.abstractions.ArmorType;
import main.java.items.armor.Plate;
import main.java.items.weapons.abstractions.WeaponCategory;
/*
 Paladins are faithful servants of the light and everything holy.
 They dispatch justice and are filled with vengeance for the wrongs done to society by evil.
 Paladins are very durable and typically wield heavy weapons.
*/
public class Paladin extends Melee {
    // Metadata
    public final ArmorType ARMOR_TYPE = ArmorType.Plate;
    public final WeaponCategory WEAPON_TYPE = WeaponCategory.BluntWeapon;

    // Base stats defensive
    private double baseHealth = CharacterBaseStatsDefensive.PALADIN_BASE_HEALTH;
    private double basePhysReductionPercent = CharacterBaseStatsDefensive.PALADIN_BASE_PHYS_RED; // Armor
    private double baseMagicReductionPercent = CharacterBaseStatsDefensive.PALADIN_BASE_MAGIC_RES; // Magic armor
    // Base stats offensive
    private double baseAttackPower = CharacterBaseStatsOffensive.PALADIN_MELEE_ATTACK_POWER;


    // Constructor
    public Paladin() {
        // When the character is created it has maximum health (base health)
        this.currentHealth = baseHealth;
    }

    // Public getters statuses and stats
    public double getCurrentMaxHealth() {
        return baseHealth;
    }  // Needs alteration after equipment is in


    // Character behaviours
    /**
     * Damages the enemy
     */
    public double attackWithBluntWeapon() {
        return this.baseAttackPower * this.weapon.getWeaponModifier() * this.weapon.getRarityModifier();
    }


}