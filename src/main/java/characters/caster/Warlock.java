package main.java.characters.caster;
// Imports
import main.java.basestats.CharacterBaseStatsDefensive;
import main.java.basestats.CharacterBaseStatsOffensive;
import main.java.characters.abstractions.Caster;
import main.java.characters.abstractions.CharacterCategory;
import main.java.items.armor.abstractions.ArmorType;
import main.java.items.armor.Cloth;
import main.java.items.weapons.abstractions.WeaponCategory;
import main.java.spells.abstractions.DamagingSpell;
import main.java.spells.abstractions.SpellCategory;
/*
 Warlocks are masters of chaos, entropy, and death. They were once mages but were corrupted by power.
 They can conjure up pure chaos energy to destroy their enemies.
*/
public class Warlock extends Caster {

    // Base stats defensive
    private double baseHealth = CharacterBaseStatsDefensive.WARLOCK_BASE_HEALTH;
    private double basePhysReductionPercent = CharacterBaseStatsDefensive.WARLOCK_BASE_PHYS_RED; // Armor
    private double baseMagicReductionPercent = CharacterBaseStatsDefensive.WARLOCK_BASE_MAGIC_RES; // Magic armor
    // Base stats offensive
    private double baseMagicPower = CharacterBaseStatsOffensive.WARLOCK_MAGIC_POWER;


    // Constructor
    public Warlock() {
        // When the character is created it has maximum health (base health)
        this.currentHealth = baseHealth;
    }

    public Warlock(DamagingSpell damagingSpell) {
        this.currentHealth = baseHealth;
        this.damagingSpell = damagingSpell;
    }

    // Public getters statuses and stats
    public double getCurrentMaxHealth() {
        return baseHealth;
    }  // Needs alteration after equipment is in

    // Character behaviours
    /**
     * Damages the enemy with its spells
     */
    public double castDamagingSpell() {
        return baseMagicPower * damagingSpell.getSpellDamageModifier() * weapon.getWeaponModifier() * weapon.getRarityModifier();
    }

}
