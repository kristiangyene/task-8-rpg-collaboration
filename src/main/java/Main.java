package main.java;

import main.java.basestats.ItemRarityModifiers;
import main.java.characters.Support.Druid;
import main.java.characters.abstractions.Character;
import main.java.characters.abstractions.CharacterType;
import main.java.characters.caster.Mage;
import main.java.characters.melee.Rogue;
import main.java.characters.ranged.Ranger;
import main.java.consolehelpers.Color;
import main.java.factories.*;
import main.java.items.armor.abstractions.Armor;
import main.java.items.armor.abstractions.ArmorType;
import main.java.items.weapons.abstractions.WeaponType;
import main.java.spells.abstractions.DamagingSpell;
import main.java.spells.abstractions.HealingSpell;
import main.java.spells.abstractions.SpellType;


public class Main {

    public static void main(String[] args) {
        System.out.println(Color.RED + "RED COLORED" +
                Color.RESET + " NORMAL");

        ArmorFactory af = new ArmorFactory();
        CharacterFactory cf = new CharacterFactory();
        EnemyFactory ef = new EnemyFactory();
        SpellFactory sf = new SpellFactory();
        WeaponFactory wf = new WeaponFactory();


        // Creating character part of 4.
        Rogue rogue = (Rogue) cf.getCharacter(CharacterType.Rogue);
        Druid druid = (Druid) cf.getCharacter(CharacterType.Druid);
        Ranger ranger = (Ranger) cf.getCharacter(CharacterType.Ranger);
        Mage mage = (Mage) cf.getCharacter(CharacterType.Mage);
        System.out.println("*Added four characters to party*");

        // Equip armor and shield for each character.
        rogue.equipArmor(af.getArmor(ArmorType.Leather, ItemRarityModifiers.COMMON_RARITY_MODIFIER));
        rogue.equipWeapon(wf.getWeapon(WeaponType.Axe, ItemRarityModifiers.COMMON_RARITY_MODIFIER));
        druid.equipArmor(af.getArmor(ArmorType.Cloth, ItemRarityModifiers.COMMON_RARITY_MODIFIER));
        druid.equipWeapon(wf.getWeapon(WeaponType.Staff, ItemRarityModifiers.COMMON_RARITY_MODIFIER));
        druid.equipSpell((HealingSpell) sf.getSpell(SpellType.Regrowth));
        ranger.equipArmor(af.getArmor(ArmorType.Mail, ItemRarityModifiers.COMMON_RARITY_MODIFIER));
        ranger.equipWeapon(wf.getWeapon(WeaponType.Bow, ItemRarityModifiers.COMMON_RARITY_MODIFIER));
        mage.equipArmor(af.getArmor(ArmorType.Cloth, ItemRarityModifiers.COMMON_RARITY_MODIFIER));
        mage.equipWeapon(wf.getWeapon(WeaponType.Wand, ItemRarityModifiers.COMMON_RARITY_MODIFIER));
        mage.equipSpell((DamagingSpell) sf.getSpell(SpellType.ArcaneMissile));
        System.out.println("*Equip starting armor and weapon*\n");

        // Test attacks.
        double rougeDamage = rogue.attackWithBladedWeapon();
        System.out.println("Rogue attack amount: " + rougeDamage);

        double rangerDamage = ranger.attackWithRangedWeapon();
        System.out.println("Ranger attack amount: " + rangerDamage);

        double mageDamage = mage.castDamagingSpell();
        System.out.println("Mage attack amount: " + mageDamage);

        // Test heal.
        double druidHeal = druid.healPartyMember();
        System.out.println("Druid heal amount: " + druidHeal + "\n");

        // Test physical and magical damage taken.

        // Test rouges attack after changing rarity to rare.
        rogue.equipWeapon(wf.getWeapon(WeaponType.Axe, ItemRarityModifiers.RARE_RARITY_MODIFIER));
        rougeDamage = rogue.attackWithBladedWeapon();
        System.out.println("*Changed the rarity for rouges weapon*");
        System.out.println("Rogue attack amount: " + rougeDamage);










    }
}
