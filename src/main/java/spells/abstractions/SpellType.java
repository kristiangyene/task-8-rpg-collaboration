package main.java.spells.abstractions;

public enum SpellType {
    ArcaneMissile,
    Barrier,
    ChaosBolt,
    Regrowth,
    Rapture,
    SwiftMend
}
