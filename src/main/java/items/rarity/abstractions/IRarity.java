package main.java.items.rarity.abstractions;

public interface IRarity {
    double powerModifier();
    String getItemRarityColor();
}
