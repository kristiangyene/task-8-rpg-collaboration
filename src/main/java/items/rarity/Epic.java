package main.java.items.rarity;

import main.java.consolehelpers.Color;
import main.java.items.rarity.abstractions.IRarity;

public class Epic implements IRarity {
    // Stat modifier
    private double powerModifier = 1.6;
    // Color for display purposes
    private String itemRarityColor = Color.MAGENTA;

    // Public properties
    @Override
    public double powerModifier() {
        return powerModifier;
    }

    @Override
    public String getItemRarityColor() {
        return itemRarityColor;
    }
}
