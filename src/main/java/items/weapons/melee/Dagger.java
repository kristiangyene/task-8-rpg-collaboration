package main.java.items.weapons.melee;

import main.java.basestats.WeaponStatsModifiers;
import main.java.items.weapons.abstractions.Weapon;

public class Dagger extends Weapon {

    // Constructors
    public Dagger() {
    }

    public Dagger(double itemRarityModifier) {
        super(WeaponStatsModifiers.DAGGER_ATTACK_MOD, itemRarityModifier);
    }
}
