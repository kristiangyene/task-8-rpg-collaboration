package main.java.items.weapons.melee;

import main.java.basestats.WeaponStatsModifiers;
import main.java.items.weapons.abstractions.Weapon;

public class Sword extends Weapon {

    // Constructors
    public Sword() {
    }

    public Sword(double itemRarityModifier) {
        super(WeaponStatsModifiers.SWORD_ATTACK_MOD, itemRarityModifier);
    }
}
