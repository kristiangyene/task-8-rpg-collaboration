package main.java.items.weapons.abstractions;

public interface IWeapon {
    double getRarityModifier();
    double getWeaponModifier();

}
