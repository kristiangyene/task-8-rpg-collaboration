package main.java.items.weapons.abstractions;

import main.java.basestats.ItemRarityModifiers;
import main.java.basestats.WeaponStatsModifiers;
import main.java.items.rarity.abstractions.ItemRarity;

public abstract class Weapon implements IWeapon {
    // Rarity
    private final double rarityModifier;
    private double weaponModifier;

    // Public properties
    @Override
    public double getRarityModifier() {
        return rarityModifier;
    }

    @Override
    public double getWeaponModifier(){ return weaponModifier; }

    // Constructors
    public Weapon() {
        this.rarityModifier = ItemRarityModifiers.COMMON_RARITY_MODIFIER;
    }

    public Weapon(double weaponStatsModifier, double itemRarityModifier) {
        this.weaponModifier = weaponStatsModifier;
        this.rarityModifier = itemRarityModifier;
    }
}
