package main.java.items.weapons.ranged;

import main.java.basestats.WeaponStatsModifiers;
import main.java.items.weapons.abstractions.Weapon;

public class Bow extends Weapon {

    // Constructors
    public Bow() {
    }

    public Bow(double itemRarityModifier) {
        super(WeaponStatsModifiers.BOW_ATTACK_MOD, itemRarityModifier);
    }
}
